#!/usr/bin/env bash
# you can use ~/.xprofile to autostart on login
export PATH="${PATH}:${HOME}/.config/herbstluftwm/sources/"
export QT_QPA_PLATFORMTHEME="qt5ct"

# If it´s not running, Run it
run() { [ ! $(pgrep $1) ] && $@& }

# If you want to use XFCE config tools
#run xfce-mcs-manager
#-- POLKIT | PAM | KEYRING
run /usr/libexec/polkit-gnome-authentication-agent-1
run /usr/lib64/libexec/kf5/pam_kwallet_init
#-- LOCKSCREEN
#run i3lock
#run xscreensaver -nosplash
#-- COMPOSITOR
#run picom 
#run picom -b --backend glx --vsync
run picom --config "${HOME}/.config/picom/default.conf"
#-- NOTIFICATION
run dunst 
#run dunst -config "${HOME}/.config/dunst/dunstrc.conf"
#-- STATUS BAR
run polybar -c "${HOME}/.config/polybar/$Ptheme/config.ini" main
#-- WIDGETS
#conky -c "${HOME}/.config/conky/"
#-- WALLPAPER
~/.fehbg								# Automatic
#mpv?									# Dynamic
#feh --no-fehbg --bg-fill "${HOME}/.config/herbstluftwm/Pics/wall.png"	# Default

#-- Demons
run redshift
run greenclip daemon
#run urxvtd
#run pcmanfm --daemon-mode
#run emacs --daemon

#-- Scripts
#run vitas
run unlock-ssh
#run wal -R
