#!/usr/bin/env bash

# Defaults
hc rule focus=on	#on=focus new clients
hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|Konsole)' focus=on

# Applications
#-Floating clients
hc rule class=Synaptic		floating=on
hc rule class=URxvt		floating=on
hc rule class=XTerm		floating=on
hc rule class=vlc		floating=on
hc rule class=dolphin		floating=on
hc rule class=MEGAsync		floating=on
hc rule class=pcloud		floating=on
hc rule class=feh		floating=on
hc rule class=Gpick		floating=on
hc rule class=kcalc		floating=on
hc rule class=qt5ct		floating=on
hc rule class=Pavucontrol	floating=on
hc rule class=ark		floating=on
#-Sub-menu
hc rule class=Firefox instance=Places	floating=on
hc rule class=Firefox instance=Browser	floating=on
#hc rule class=VSCodium instance=.. floating=on
#-On tags
hc rule class=kitty		tag=1 focus=on switchtag=on index=1
hc rule class=Firefox		tag=2 focus=on switchtag=on index=2
hc rule class=Chromium-browser	tag=2 focus=on switchtag=on index=2
hc rule class=Pcmanfm		tag=3 focus=on switchtag=on index=3
hc rule class=VSCodium		tag=3 focus=on switchtag=on index=3
hc rule class=discord		tag=4 focus=on switchtag=on index=4
hc rule class=Gimp-2.10		tag=4 focus=on switchtag=on index=4
hc rule class=TelegramDesktop	tag=5 focus=on switchtag=on index=5

# Dialog
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' floating=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG'		focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off

hc set tree_style '╾│ ├└╼─┐'
