#!/usr/bin/env bash

# Basic
hc keybind $Mod-Shift-q	quit
hc keybind $Mod-Shift-r	reload
hc keybind $Mod-q	close

# Focus
#hc keybind $Mod-BackSpace	cycle_monitor
hc keybind $Mod-Tab		cycle_all +1
hc keybind $Mod-Shift-Tab	cycle_all -1
hc keybind $Mod-c		cycle
hc keybind $Mod-i		jumpto urgent

# Focusing clients
hc keybind $Mod-Left	focus left
hc keybind $Mod-Down	focus down
hc keybind $Mod-Up	focus up
hc keybind $Mod-Right	focus right
hc keybind $Mod-h	focus left
hc keybind $Mod-j	focus down
hc keybind $Mod-k	focus up
hc keybind $Mod-l	focus right

# Moving clients
hc keybind $Mod-Shift-Left	shift left
hc keybind $Mod-Shift-Down	shift down
hc keybind $Mod-Shift-Up	shift up
hc keybind $Mod-Shift-Right	shift right
hc keybind $Mod-Shift-h		shift left
hc keybind $Mod-Shift-j		shift down
hc keybind $Mod-Shift-k		shift up
hc keybind $Mod-Shift-l		shift right

# Theme
#hc keybind $Mod-t	increase_frame
#hc keybind $Mod-t	decrease_frame
#hc keybind $Mod-t	increase_gap
#hc keybind $Mod-t	decrease_gap

# Mouse
hc mousebind $Mod-Button1	move
hc mousebind $Mod-Button3	zoom
hc mousebind $Mod-Button2	resize
hc mousebind $Alt-Button1	resize
