#!/usr/bin/env bash

# Splitting frames
hc keybind $Mod-u	split   bottom  0.5
hc keybind $Mod-o	split   right   0.5
# let the current frame explode into subframes
hc keybind $Mod-Control-space split explode

# Resizing frames and floating clients
resizestep=0.02
hc keybind $Mod-Control-h       resize left +$resizestep
hc keybind $Mod-Control-j       resize down +$resizestep
hc keybind $Mod-Control-k       resize up +$resizestep
hc keybind $Mod-Control-l       resize right +$resizestep
hc keybind $Mod-Control-Left    resize left +$resizestep
hc keybind $Mod-Control-Down    resize down +$resizestep
hc keybind $Mod-Control-Up      resize up +$resizestep
hc keybind $Mod-Control-Right   resize right +$resizestep

# Layouting
hc keybind $Mod-r remove
hc keybind $Mod-s floating toggle
hc keybind $Mod-f fullscreen toggle
hc keybind $Mod-Shift-f set_attr clients.focus.floating toggle
hc keybind $Mod-p pseudotile toggle

#hc keybind $Mod-d	max

hc keybind $Mod-space						\
        or , and . compare tags.focus.curframe_wcount = 2	\
        . cycle_layout +1 vertical horizontal max grid		\
        , cycle_layout +1
