*last updated: 25 January 21*<br>
*version: 0.9 compiled from source*

<img src="Pics/hl_banner.png" align=center height=300px>

## About this window manager

<img src="Pics/hldemo_blueg.png" align=right width=350px>
<p align=left>
<strong><a href="https://herbstluftwm.org/">Herbstluftwm</a></strong> is a manual tiling window manager for x11. Is characterized by the layout which is based on splitting frames into subframes and the ability to multi monitor setup with X tags per monitor.<br>
It's also written from scratch in C and configured with a bash script.<br>
Also is very easy to use and customizable with bash scripts.
</p>

`Details`
+ **Os**: Debian '10' Buster
+ **Terminal**: konsole
+ **Shell**: zsh + powerlevel10k
+ **Bar**: Polybar
+ **Applications**: neofetch, htop, feh, colorls
+ **Compositor**: Picom

### Updates
Work in progress..

<!-- [Video/gif of demostration] -->

## Table of content
+ [Features](#Features)
+ [Gallery](#Gallery)
+ [Dependences](#Dependences)
	- [Requiered](#Requiered-dependences)
	- [Optional](#Optional-dependences)
+ [Install](#Installation)
+ [File structure](#File-structure)
+ [To Do](#To-Do)
+ [Issues](#Issues)

---

## Features
yeah... soon..

## Gallery
**Fruit**
<p align="center">
  <img src="Pics/fruit1.png" alt="Main screenshot"/>
  <img src="Pics/fruit2.png" width="450px" height="260px" alt="Popup screenshot"/>
  <img src="Pics/fruit3.png" width="450px" height="260px" alt="Sentence screenshot"/>
</p>
<p align="center">
  <sub>
  	<b>OS</b>: Debian | <b>Bar</b>: Polybar | <b>Theme</b>: Default | <b>Wallpaper</b>: .. <br>
	<b>Applications</b>: htop - neofetch - feh - konsole
  </sub>
</p>

**Blueg**
<p align="center">
  <img src="Pics/blueg1.png" alt="Main screenshot"/>
  <img src="Pics/blueg2.png" width="340px" height="200px" alt="fruit 2"/>
  <img src="Pics/blueg3.png" width="340px" height="200px" alt="fruit 3"/>
  <img src="Pics/blueg4.png" width="340px" height="200px" alt="fruit 4"/>
</p>
<p align="center">
  <sub>
  	<b>OS</b>: Debian | <b>Bar</b>: Polybar | <b>Theme</b>: blueg | <b>Wallpaper</b>: .. <br>
	<b>Applications</b>: urxvt - feh - vim - zsh + P10K
  </sub>
</p>

## Dependences
### Requiered dependences
* **Herbstluftwm** obviously
* **Feh**        -> Set the wallpaper
* **Polybar**    -> Status bar
* **Dunst**      -> Notificacionts
* **Rofi**       -> Launcher
* **Picom**      -> Compositor
* **i3-lock**    -> Lock the screen

Herbstluft come with dzen2 as default bar but i instead use polybar.<br>
Polybar requiere [aditional dependences](https://github.com/yowls/pbar), based in what theme you choose.

### Optional dependences
* **Greenclip** for clipboard manager with Rofi
* **Maim** for screenshots
* **MPD** - **MPC** for media player CLI
* **Playerctl** for multiple media control, include spotify
* **xclip** for multiple clipboard operations
* **xbacklight** for brightness control
* **TLP** for power managment

### Fonts
Needed fonts for the setup:
* .. for general setup
* .. for rofi
* [Any NerdFont](https://github.com/ryanoasis/nerd-fonts) for terminal

## Installation
To use you have 2 options, all in one or use the 8 files.<br>
For the all in one just rename as autostart in .config/herbstluftwm and thats all.<br>
For the 8 files just follow the instructions below.

In downloads folder or any other do:

```bash
	$ git clone https://gitlab.com/yowls/hlwm ~/.config/herbstluftwm
	$ rm -r ~/.config/herbstluftwm/.git ~/.config/herbstluftwm/Pics   #Remove unnecessary things(Optional)
```

## File structure
Explanation about what do every file.<br>
They are written in singular by ease by naming them and sorted according to who starts first in the [main file](https://gitlab.com/yowls/hlwm/-/blob/master/autostart).<br>
Herbstluftwm start in 8 simple steps :)<br>
Because it is so divided, it may take a litte more time to start completely. Anyway you can use the all-in-one file: [autostart\_full](https://gitlab.com/yowls/hlwm/blob/master/autostart_full).
***btw,All the keybinds are not only in keybind file***

### Main file
**File name: "autostart"**

Is like the main function to start the wm.<br>
Just clear previous config and call the other files.<br>
Dont kill previous programs instances like picom or polybar, just clear the wm options.

### Variables
**File name: "vars.sh"**

For easier preference control just modify this file.<br>
Here you **set preferred applications and theme for herbstluft and polybar**

Actuall preferences
+ **Herbstluft theme**: her
+ **Polybar theme**: her
+ **Browser**: Firefox
+ **File explorer**: Pcmanfm
+ **Editor**: Vim
+ **Terminal**: Urxvt
+ **Alternative terminal**: Kitty

### Window
**File name: "window.sh"**

Set the **keybind** to manage:<br>
**Focus, Moving, Resize and Mouse keybinds.**<br>
In general is for window manipulation.

### Layout and frames
**File name: "layout.sh"**

Refers to frames and layout of wm control

### Workspaces
**File name: "workspace.sh"**

Set all the virtual workspaces and the keybinds to switch to these.<br>
Originally called TAGS, but since they take care of the virtual desks, its generic name would be.
You can adjust as many as you want.<br>
There are currently 10.

### Keybinds
**File name: "keybind.sh"**

Here are the keybinds for **launch programs and scripts** but not to set the keybinds for window or frames.<br>
Preferences about default terminal,browser,etc are in [vars file](https://gitlab.com/yowls/hlwm/blob/master/vars.sh)
See for manipulation of [window](https://gitlab.com/yowls/hlwm/blob/master/window.sh) and [frames,layouts](https://gitlab.com/yowls/hlwm/blob/master/layout.sh).<br>

#### Some keybinds
*From not only keybind file.*

| Cause                                  | Consequence          |
| -------------------------------------- | -------------------- |
| <kbd>Superkey</kbd> + <kbd>Enter</kbd> | Launch Terminal      |
| <kbd>Superkey</kbd> + <kbd>b</kbd>     | Launch Browser       |
| <kbd>Superkey</kbd> + <kbd>e</kbd>     | Launch file explorer |
| <kbd>Alt</kbd> + <kbd>Space</kbd>      | Launch rofi          |
| <kbd>Print</kbd>                       | Select an area and copy to clipboard |
| <kbd>Superkey</kbd> + <kbd>q</kbd>     | Close window         |

### Theme
**File name: "Themes/'theme'.sh"**

Does that, set the theme that herbstluftwm will going to use.<br>
You can change the theme just renaming [Here](https://gitlab.com/yowls/hlwm/blob/master/vars.sh#L8)<br>
Or you can create one and put in that folder then rename the same file

*For more information about themes, enter in the* [Herbstluft Themes folder](https://gitlab.com/yowls/hlwm/tree/master/Themes)

### Rules
**File name: "rule.sh"**

Sets the rules that windows must comply.<br>
To pick a application name, execute `xprop | grep 'CLASS'`<br>
¿How to break the rules some times?

#### Some rules
* open browser in 2nd workspace
* 3rd workspace is floating by default
* urxvt and pcmanfm is set to floating window

### Startup programs
**File name: "startup.sh"**

Run all the programs that start on login.<br>
Some of them are deamons, others are programs like the browser,compositor,etc. In general, for other software that are not herbsluft config.
If you want to add a program, put **"run"** at the beginning of the line, because it is verified that it is not running. This is defined in the same function run.
Otherwise, if you have, for example, the terminal open and reloads, it will spawn again as if it were starting.

### Bar

In this case, i use polybar as bar, so herbstluft call this file to set the config.<br>
The call is produced in [here](https://gitlab.com/yowls/hlwm/-/blob/master/startup.sh#L24)<br>
But for change theme [rename this line](https://gitlab.com/yowls/hlwm/blob/master/vars.sh#L9)
*For more information about all the themes availables enter in the* [Polybar Folder](https://github.com/yowls/pbar)

You may not use Polybar, and use the bar you want, such as dzen2,tint2, etc. Just delete [This Line](https://gitlab.com/yowls/hlwm/-/blob/master/startup.sh#L24) and replace it with the program of your choice.<br>
Remember add *run* at the beginning so you don't have trouble reloading the wm.

### Sources
In this folder will be the scripts files.


## To Do
* Better system to check the keybind config when creating a new one

## Issues
* The maim command set in [startup.sh](https://gitlab.com/yowls/hlwm/-/blob/master/keybind.sh#L34) dont renovate the name every time i take a new screenshot
