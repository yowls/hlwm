#!/usr/bin/env bash

# SYSTEM
#--- Lock session
hc keybind $Mod-l	spawn i3-lock
#--- Volume control
hc keybind XF86AudioRaiseVolume	spawn pactl set-sink-volume @DEFAULT_SINK@ +5%
hc keybind XF86AudioLowerVolume	spawn pactl set-sink-volume @DEFAULT_SINK@ -5%
hc keybind XF86AudioMute	spawn pactl set-sink-mute @DEFAULT_SINK@ toggle
#--- Microphone
hc keybind XF86AudioMicMute	spawn pactl set-source-mute @DEFAULT_SOURCE@ toggle
#--- Music control
hc keybind XF86AudioNext	spawn playerctl next
hc keybind XF86AudioPrev	spawn playerctl previous
hc keybind XF86AudioPlay	spawn playerctl play-pause
#--- Brigthness control
hc keybind XF86MonBrightnessUp		spawn xbacklight -inc 5
hc keybind XF86MonBrightnessDown	spawn xbacklight -dec 5

# PREFERED APPLICATIONS
hc keybind $Mod-Return		spawn $Terminal
hc keybind $Alt-Control-t	spawn $Terminal2
#hc keybind $Mod-v	spawn $Editor
#hc keybind $Mod-v	spawn $Editor_cli
#hc keybind $mod-p	spawn $Config
#hc keybind $mod-o	spawn $Config_dir
hc keybind $Mod-b	spawn $Browser
hc keybind $Mod-e	spawn $Explorer

# APPLICATIONS
#hc keybind $Mod-c	spawn emacs

#-Toggle bar (polybar)
#hc keybind $Alt-p	spawn ..

#-SCREENSHOTS (maim) (careful with keybind conflict)
# Save selected area in clipboard
hc keybind Print	spawn maim -s | xclip -selection clipboard -t image/png
# Save an selected area in downloads
hc keybind $Alt-Print	spawn maim -s ~/Downloads/$(date +%s).png
# Save a full Screenshot in desktop
hc keybind $Mod-Print	spawn maim ~/Desktop/$(date +%s).png
# Open a selected area with feh
#hc keybind $Alt-Print	spawn maim -s | feh -

#-RECORD (?)
#hc keybind ..

# Launcher (rofi)
hc keybind $Alt-space	spawn rofi -show drun combi
hc keybind $Alt-x	spawn rofi -show run

# Rofi-scripts
#-Logout prompt
hc keybind $Alt-l	spawn rofi -show power-menu -modi power-menu:rofi-power-menu
#-Clipboard manager(greenclip)
hc keybind $Alt-m	spawn rofi -modi "clipboard:greenclip print" -show clipboard -run-command '{cmd}'
hc keybind $Alt-n	spawn pkill greenclip && greenclip clear && greenclip daemon &
#-Translate(rofitr)
#hc keybind $Alt-t	spawn rofitr
#-Calculator
#hc keybind $Alt-t	spawn rofi-calc
