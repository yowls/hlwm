#!/usr/bin/env bash

# SPECIAL KEYS
Mod="Mod4"	# Use the SUPER KEY as the main modifier
Alt="Mod1"	# Use the ALT KEY as second modifier
AltR="Mod3"	# RIGHT alt key

# THEMING
Htheme="Minla"	# Herbstluft Theme
Ptheme="Her"	# Polybar Theme

# PREFERRED APPLICATIONS

Terminal="kitty"	# Main terminal
Terminal2="urxvt"	# Alternative

Editor="codium"			# Graphical text editor
Editor_cli="$Terminal -e nvim"	# CLI text editor

Config="$Editor_cli ~/.config/herbstluftwm/autostart"
Config_dir="$Editor_cli ~/.config/herbstluftwm"

Explorer="pcmanfm"
Browser="firefox"
