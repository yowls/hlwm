#!/usr/bin/env bash

# Code name: Her
# "quote about her"

# Color Scheme
#color0="#414451"
#color1="#cdeac0"
#color2="#efe9ae"
#color3="#fec3a6"
#color4="#ffac81"
#color5="#3e5c76"

xsetroot -solid '#3e5c76'
hc set frame_border_active_color '#aed9e0'
hc set frame_border_normal_color '#414451'
hc set frame_bg_normal_color 'transparent'
hc set frame_bg_active_color 'transparent'
hc set frame_border_width 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 2
hc set always_show_frame 0

hc attr theme.active.color '#f08080'
hc attr theme.normal.color '#414451'
hc attr theme.urgent.color '#efe9ae'
hc attr theme.inner_width 0
hc attr theme.inner_color '#414451'
hc attr theme.border_width 3
hc attr theme.floating.border_width 3
hc attr theme.floating.outer_width 0
hc attr theme.floating.outer_color '#414451'
hc attr theme.active.inner_color '#fec3a6'
hc attr theme.active.outer_color '#ffac81'
hc attr theme.background_color '#414451'

#hc attr theme.padding_top 3
#hc attr theme.padding_bottom 2
#hc attr theme.padding_right 0
#hc attr theme.padding_left 0

hc set window_gap 2
hc set frame_padding 0
hc pad :0 16 0 0 0

#hc set smart_window_surroundings off
#hc set smart_frame_surroundings on
#hc set mouse_recenter_gap 0
