#!/usr/bin/env bash

# Code name: Slate

# Color Scheme
#color0="#212121"
#color1="#0f3754"
#color2="#254a49"
#color3="#8bdee0"
#color4="#a380d3"
#color5="#c4c9bf"

xsetroot -solid '#0f3754'
hc set frame_border_active_color '#a380d3'
hc set frame_border_normal_color '#212121'
hc set frame_bg_normal_color 'transparent'
hc set frame_bg_active_color 'transparent'
hc set frame_border_width 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 2
hc set always_show_frame 0

hc attr theme.active.color '#a380d3'
hc attr theme.normal.color '#212121'
hc attr theme.urgent.color '#8bdee0'
hc attr theme.inner_width 0
hc attr theme.inner_color '#212121'
hc attr theme.border_width 3
hc attr theme.floating.border_width 3
hc attr theme.floating.outer_width 0
hc attr theme.floating.outer_color '#212121'
hc attr theme.active.inner_color '#0f3754'
hc attr theme.active.outer_color '#254a49'
hc attr theme.background_color '#212121'

#hc attr theme.padding_top 3
#hc attr theme.padding_bottom 2
#hc attr theme.padding_right 0
#hc attr theme.padding_left 0

hc set window_gap 2
hc set frame_padding 0
hc pad :0 16 0 0 0

#hc set smart_window_surroundings off
#hc set smart_frame_surroundings on
#hc set mouse_recenter_gap 0
