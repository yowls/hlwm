#!/usr/bin/env bash

# Code name: Slate

# Color Scheme
#color0="#212121"
#color1="#e83a3f"
#color2="#7aba39"
#color3="#f5971d"
#color4="#134eb2"
#color5="#0e707c"

xsetroot -solid '#0e707c'
hc set frame_border_active_color '#134eb2'
hc set frame_border_normal_color '#212121'
hc set frame_bg_normal_color 'transparent'
hc set frame_bg_active_color 'transparent'
hc set frame_border_width 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 2
hc set always_show_frame 0

hc attr theme.active.color '#134eb2'
hc attr theme.normal.color '#212121'
hc attr theme.urgent.color '#e83a3f'
hc attr theme.inner_width 0
hc attr theme.inner_color '#212121'
hc attr theme.border_width 3
hc attr theme.floating.border_width 3
hc attr theme.floating.outer_width 0
hc attr theme.floating.outer_color '#212121'
hc attr theme.active.inner_color '#7aba39'
hc attr theme.active.outer_color '#f5971d'
hc attr theme.background_color '#212121'

#hc attr theme.padding_top 3
#hc attr theme.padding_bottom 2
#hc attr theme.padding_right 0
#hc attr theme.padding_left 0

hc set window_gap 2
hc set frame_padding 0
hc pad :0 16 0 0 0

#hc set smart_window_surroundings off
#hc set smart_frame_surroundings on
#hc set mouse_recenter_gap 0
