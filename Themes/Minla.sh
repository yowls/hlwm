#!/usr/bin/env bash

# Code name: Minla

# Color Scheme
#color0="#786fa6"
#color1="#546de5"
#color2="#38ada9"
#color3="#f7d794"
#color4="#f3a683"
#color5="#ff4d4d"

xsetroot -solid '#333641'
hc set frame_border_active_color '#778beb'
hc set frame_border_normal_color '#786fa6'
hc set frame_bg_normal_color 'transparent'
hc set frame_bg_active_color 'transparent'
hc set frame_border_width 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 2
hc set always_show_frame 0

hc attr theme.active.color '#778beb'
hc attr theme.normal.color '#786fa6'
hc attr theme.urgent.color '#ff4d4d'
hc attr theme.inner_width 0
hc attr theme.inner_color '#786fa6'
hc attr theme.border_width 3
hc attr theme.floating.border_width 3
hc attr theme.floating.outer_width 0
hc attr theme.floating.outer_color '#786fa6'
hc attr theme.active.inner_color '#f7d794'
hc attr theme.active.outer_color '#546de5'
hc attr theme.background_color '#786fa6'

#hc attr theme.padding_top 3
#hc attr theme.padding_bottom 2
#hc attr theme.padding_right 0
#hc attr theme.padding_left 0

hc set window_gap 2
hc set frame_padding 0
hc pad :0 16 0 0 0

#hc set smart_window_surroundings off
#hc set smart_frame_surroundings on
#hc set mouse_recenter_gap 0
